#!/usr/bin/env python3

import json
import os
import os.path
import sys
import time

from d3m.container import dataset as dataset_module


def search_directory(datasets_directory):
    datasets_directory = os.path.abspath(datasets_directory)

    for dirpath, dirnames, filenames in os.walk(datasets_directory, followlinks=True):
        if 'datasetDoc.json' in filenames:
            # Do not traverse further (to not parse "datasetDoc.json" if they
            # exists in raw data filename).
            dirnames[:] = []

            dataset_doc_path = os.path.join(dirpath, 'datasetDoc.json')

            print("Processing '{dataset_doc_path}'.".format(dataset_doc_path=dataset_doc_path))

            try:
                before = time.perf_counter()
                dataset_digest = dataset_module.get_d3m_dataset_digest(dataset_doc_path)
                after = time.perf_counter()
            except Exception as error:
                raise RuntimeError("Unable to compute digest for dataset '{dataset_doc_path}'.".format(dataset_doc_path=dataset_doc_path)) from error

            try:
                with open(dataset_doc_path, 'r') as dataset_doc_file:
                    dataset_doc = json.load(dataset_doc_file)

                if dataset_doc['about'].get('digest', None) == dataset_digest:
                    print("Digest match (took {time:.3f}s).".format(time=after - before))
                    continue

                dataset_doc['about']['digest'] = dataset_digest

                with open(dataset_doc_path, 'w') as dataset_doc_file:
                    # In Python 3.6+ order of values in dicts is preserved and we use
                    # this here to try to minimize the line-by-line changes of JSON files.
                    json.dump(dataset_doc, dataset_doc_file, ensure_ascii=False, indent=2)

                print("Digest updated (took {time:.3f}s).".format(time=after - before))

            except Exception as error:
                raise RuntimeError("Unable to update digest for dataset '{dataset_doc_path}'.".format(dataset_doc_path=dataset_doc_path))


def main():
    datasets_directories = sys.argv[1:] or ['.']

    for datasets_directory in datasets_directories:
        search_directory(datasets_directory)


if __name__ == '__main__':
    main()
